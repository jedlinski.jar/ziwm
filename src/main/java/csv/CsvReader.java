package csv;

import java.io.IOException;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.util.ClassPathResource;

/**
 * Created by matio on 25.03.2017.
 */
public class CsvReader {
    int numLinesToSkip = 0;
    String delimiter = ",";
    RecordReader recordReader = new CSVRecordReader(numLinesToSkip,delimiter);
    public RecordReader readFile(String file) throws IOException, InterruptedException {
        recordReader.initialize(new FileSplit(new ClassPathResource(file).getFile()));

        return recordReader;
    }
}
