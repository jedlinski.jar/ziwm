package csv;

/**
 * Created by szef on 25.03.17.
 */
public class ConfigurationConstants {

	static int LINES_TO_SKIP = 0;
	static int LABEL_INDEX = 9;		//ktora kolumna opisuje labele (klasy)
	static int CLASSES = 2;			//ile jest klas
	static int BATCH_SIZE = 699;	//ile zestawow danych zaladowac z pliku do DataSetu
	static int INPUTS = 6;
	static int HIDDEN_INPUTS=9;
	static int OUTPUTS = CLASSES;
	static int ITERATIONS = 1000;
	static int SEED = 10;
	static int PRINT_ITERATIONS = 100;
	static double PERCENT_TRAIN = 0.5;	//ile % danych ma byc uzyte do trenowania sieci
	static int NUMBER_OF_REPEATS = 5;
	static int NUMBER_OF_ALL_REPEATS = 5;
	static double MOMENTUM =0.8;
	static double LEARNING_RATE =0.7;
}
