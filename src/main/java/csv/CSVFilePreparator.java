package csv;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;


/**
 * Created by szef on 26.03.17.
 */
public class CSVFilePreparator {

	private static final String TMP_FILE_NAME = "temp.csv";

	private static List<Integer> ORDER_OF_FEATURES = asList(6,1,2,7,4,3,5,8,9);

	static String prepare(String fileName, int numberOfFeatures) throws IOException {

		Path filePath = Paths.get(fileName);

		Stream<String> lines = Files.lines(filePath)
									.map(l->processSingleLine(l, numberOfFeatures));

		Path tempFile = Paths.get(TMP_FILE_NAME);

		Files.write(tempFile, (Iterable<String>) lines::iterator);

		return TMP_FILE_NAME;
	}

	private static String processSingleLine(final String line,int arguments) {


		String newLine = ORDER_OF_FEATURES.stream()
				.map(n -> getColumn(n,line))
				.limit(arguments)
				.collect(joining(",", "", ","));

		newLine = replaceQuestionMark(newLine);
		newLine += getClassId(line);

		return newLine;
	}

	private static String getColumn(int id,String line){
		return (line.split(","))[id-1];
	}

	private static String replaceQuestionMark(String line) {

		return line.replace("?", "4");
	}

	private static String getClassId(String line) {

		if (line.endsWith("2")) {
			return  "0";
		}

		return "1";
	}
}
