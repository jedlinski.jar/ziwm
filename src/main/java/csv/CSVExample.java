package csv;

import static csv.ConfigurationConstants.BATCH_SIZE;
import static csv.ConfigurationConstants.CLASSES;
import static csv.ConfigurationConstants.HIDDEN_INPUTS;
import static csv.ConfigurationConstants.INPUTS;
import static csv.ConfigurationConstants.ITERATIONS;
import static csv.ConfigurationConstants.LEARNING_RATE;
import static csv.ConfigurationConstants.LINES_TO_SKIP;
import static csv.ConfigurationConstants.MOMENTUM;
import static csv.ConfigurationConstants.NUMBER_OF_ALL_REPEATS;
import static csv.ConfigurationConstants.NUMBER_OF_REPEATS;
import static csv.ConfigurationConstants.OUTPUTS;
import static csv.ConfigurationConstants.PERCENT_TRAIN;
import static csv.ConfigurationConstants.PRINT_ITERATIONS;
import static org.datavec.api.records.reader.impl.csv.CSVRecordReader.DEFAULT_DELIMITER;
import static org.deeplearning4j.nn.conf.Updater.NESTEROVS;
import static org.nd4j.linalg.activations.Activation.SIGMOID;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.FeedForwardLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
import org.nd4j.linalg.factory.Nd4j;

import sun.plugin.dom.exception.InvalidStateException;


/**
 * Created by matio on 25.03.2017.
 */
public class CSVExample {

    private static final String CSV_DATA_FILE = "breast-cancer-wisconsin.csv";

    public static void main(String[] args) throws Exception {


        List<Double> averageAccuracy = new ArrayList<>();
        List<Double> averageVariance = new ArrayList<>();

        Map<Number, Double> inputsToAccuracy = new HashMap<>(INPUTS * 2);
        Map<Number, Double> inputsToVariance = new HashMap<>(INPUTS * 2);
        for (int inputs = 1; inputs <= 9; inputs++) {
            List<Double> accuracies = new ArrayList<>(NUMBER_OF_REPEATS * NUMBER_OF_ALL_REPEATS * 2);
            for (int j = 0; j < NUMBER_OF_ALL_REPEATS; j++) {
                MultiLayerConfiguration conf = prepareMultiLayerConfiguration(inputs, HIDDEN_INPUTS, MOMENTUM, LEARNING_RATE);
                MultiLayerNetwork network = new MultiLayerNetwork(conf);
                network.init();
                network.setListeners(new ScoreIterationListener(PRINT_ITERATIONS));

                for (int i = 0; i < NUMBER_OF_REPEATS; i++) {
                    System.err.println("We are in iteration :" + i + "of : " + j + " for : " + inputs);
                    SplitTestAndTrain testAndTrain = prepareData(inputs);

                    DataSet trainingData = testAndTrain.getTrain();
                    DataSet testData = testAndTrain.getTest();

                    DataNormalization normalizer = new NormalizerStandardize();
                    normalizer.fit(trainingData);
                    normalizer.transform(trainingData);
                    normalizer.transform(testData);

                    double firstAccuracy = learnAndTest(trainingData, testData, network);
                    double secondAccuracy = learnAndTest(testData, trainingData, network);

                    accuracies.add(firstAccuracy);
                    accuracies.add(secondAccuracy);
                }

            }
            double avarage = countAverage(accuracies);
            inputsToAccuracy.put(inputs, avarage);
            double variance = countVariance(avarage, accuracies);
            inputsToVariance.put(inputs, variance);
        }
        System.out.println("------------------SCORES-------------------");
        System.out.println("-------------------------------------------");
        System.out.println("------------------ACCURACY-------------------");
        inputsToAccuracy.entrySet()
                .stream()
                .forEach(e -> System.out.println("" + e.getKey() + " : " + e.getValue()));
        System.out.println("-------------------------------------------");
        System.out.println("------------------VARIANCE-------------------");
        inputsToVariance.entrySet()
                .stream()
                .forEach(e -> System.out.println("" + e.getKey() + " : " + e.getValue()));


    }

    private static double countVariance(double avarage, List<Double> accuracies) {

		/*return accuracies.stream()
                .mapToDouble(accuracy -> (accuracy-avarage)*(accuracy-avarage))
				.sum();*/
        double sum = 0.0;
        for (Double accuracy : accuracies) {
            sum += (accuracy - avarage) * (accuracy - avarage);
        }
        return sum;
    }

    private static double countAverage(List<Double> accuracies) {

        return accuracies.stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElseThrow(() -> new InvalidStateException("List of accuracies contains no values"));
    }

    private static double learnAndTest(DataSet trainingData, DataSet testData, MultiLayerNetwork network) {

        network.fit(trainingData);

        //evaluate the model on the test set
        Evaluation eval = new Evaluation(CLASSES);
        INDArray output = network.output(testData.getFeatureMatrix());

        eval.eval(testData.getLabels(), output);

        return eval.accuracy();
    }

    private static MultiLayerConfiguration prepareMultiLayerConfiguration(int inputs, int numberOfHiddenInputs, double momentum, double leariningRate) {

        return new NeuralNetConfiguration.Builder()
                .iterations(ITERATIONS)
                .activation(SIGMOID)
                .weightInit(WeightInit.RELU)
                .updater(NESTEROVS)
                .learningRate(leariningRate)
                .momentum(momentum)
                .list(
                        generateMiddleLayer(inputs, numberOfHiddenInputs),
                        generateOutputLayer(numberOfHiddenInputs, OUTPUTS))
                .backprop(true)
                .pretrain(false)
                .build();
    }

    private static SplitTestAndTrain prepareData(int inputs) throws IOException, InterruptedException {

        String preparedFile = CSVFilePreparator.prepare(CSV_DATA_FILE, inputs);
        RecordReader recordReader = new CSVRecordReader(LINES_TO_SKIP, DEFAULT_DELIMITER);
        recordReader.initialize(new FileSplit(new File(preparedFile)));
        DataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, BATCH_SIZE, inputs, CLASSES);
        DataSet allData = iterator.next();
        allData.shuffle();

        return allData.splitTestAndTrain(PERCENT_TRAIN);
    }

    private static OutputLayer generateOutputLayer(int inputs, int outputs) {

        return new OutputLayer.Builder()
                .activation(SIGMOID)
                .nIn(inputs)
                .nOut(outputs)
                .build();
    }

    private static FeedForwardLayer generateMiddleLayer(int inputs, int outputs) {
        return new DenseLayer.Builder()
                .nIn(inputs)
                .nOut(outputs)
                .activation(SIGMOID)
                .build();
    }

    private static void PlotTheData(DataSet trainingData, DataSet testData, MultiLayerNetwork model) {
        //-------------------WYKRES-------------------------

        //Plot the data:
        double xMin = -1.5;
        double xMax = 2.5;
        double yMin = -1.0;
        double yMax = 2.5;

        //Let's evaluate the predictions at every point in the x/y input space
        int nPointsPerAxis = 100;
        double[][] evalPoints = new double[nPointsPerAxis * nPointsPerAxis][9];
        int count = 0;
        for (int i = 0; i < nPointsPerAxis; i++) {
            for (int j = 0; j < nPointsPerAxis; j++) {
                double x = i * (xMax - xMin) / (nPointsPerAxis - 1) + xMin;
                double y = j * (yMax - yMin) / (nPointsPerAxis - 1) + yMin;

                evalPoints[count][0] = x;
                evalPoints[count][1] = y;

                count++;
            }
        }

        INDArray allXYPoints = Nd4j.create(evalPoints);
        INDArray predictionsAtXYPoints = model.output(allXYPoints);

        PlotUtil.plotTrainingData(trainingData.getFeatures(), trainingData.getLabels(), allXYPoints, predictionsAtXYPoints, nPointsPerAxis);

        INDArray testPredicted = model.output(testData.getFeatures());
        PlotUtil.plotTestData(testData.getFeatures(), testData.getLabels(), testPredicted, allXYPoints, predictionsAtXYPoints, nPointsPerAxis);

        System.out.println("****************Example finished********************");
    }

}


